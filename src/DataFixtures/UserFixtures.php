<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail('johndoe@mail.com')
            ->setPassword('$2y$13$PzWLln.AaA1ygSt10yn9GuJGWPlt9tmS5QeOKrBzYNDUjNFXvgPw.');
        $manager->persist($user);

        $manager->flush();
    }
}
